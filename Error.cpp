/*
Name :  Error.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2016. All rights reserved.
Date : 20-06-2016
Description:
This module produces Errors when called from different modules. The expression parser algorithm would generate Errors and check Errors 
stored by this module. The Error module would add all Errors into a std::vector of std::strings . 
The expression parser checks if this vector is empty before executing. Errors listed in this vector of std::strings would be 
displayed after stopping program execution.
 
Notes:

Bug and revision history;
0.1 Initial version
0.2 Added line numbers at which Errors occur
0.3 Changed some function parameters to const data type & 


Change the error tracer mechanism to collect all errors during execution of all function and print them all at once. 
Change the data string to a struct contaning a string.
*/
#include "Error.h"
#include "TestFramework.h"
/* List all Errors in a vector of strings; */
/* Change this to vector of struct containing string, error type, error number */
static std::vector <std::string> ErrorsList;
// Comment to integrate with all modules.
#if(0)
SINT32 main(void)
{
    ErrorTracer(MISSING_COMMENT, 1, 1);
    ErrorTracer(INCORRECT_DATA_DEFINITION, 2, 1);
    ErrorTracer(INCORRECT_DATA_DECLARATION, 3, 1);
    ErrorTracer( MISSING_FUNCTION_ARGS, 4, 1);
    ErrorTracer(INCORRECT_FUNCTION_DEFINITION, 5, 1);
    ErrorTracer(ILLEGAL_CHARACTER, 6, 1);
    ErrorTracer(MISSING_LEFT_PARENTHESIS, 7, 1);
    ErrorTracer(MISSING_RIGHT_PARENTHESIS, 7, 1);
    ErrorTracer(EMPTY_PARENTHESES , 7, 1);
    ErrorTracer(MISSING_OPERAND, 7, 1);
    ErrorTracer(MISSING_OPERATOR, 7, 1);
    ErrorTracer(INVALID_IDENT_KEYWORD_USED, 7, 1);
    ErrorTracer(STACK_OVERFLOW, 8, 1);
    ErrorTracer(MEMORY_ALLOCATION_ERROR, 9, 1);
    ErrorTracer(UNABLE_TO_PROCESS_LINENUMBER, 10, 1);
    ErrorTracer(DEALLOCATING_MEMORY_FOR_DELETED_NODE, 11, 1); /* Possible Runtime Error. Write code to avoid this Error.*/
    ErrorTracer(CONSECUTIVE_MULTIPLICATION_OPERATORS, 12, 1);
    ErrorTracer(CONSECUTIVE_DIVIDE_OPERATORS, 13, 1);
    ErrorTracer(INCORRECT_ARITHMETIC, 14, 1);
    ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
    ErrorTracer(EMPTY_EXPRESSION,0,0);
    ErrorTracer(BITWISEARITHMETIC_ON_FLOATINGPOINTNUMBERS,0,0);
    ErrorTracer(MACRO_DEFINITION_DECLARATION_DONT_MATCH,0,0);
    ErrorTracer(MACRO_DEFINITION_EXPRESSION_DONT_MATCH,0,0);
    ErrorTracer(MACRO_MISSING_IDENTS,0,0);
    ErrorTracer(MACRO_MISSING_OPERANDS,0,0);
    ErrorTracer(MACRO_MISSING_DECLARATION,0,0);
    ErrorTracer(MACRO_MISSING_DEFINITION,0,0);

    PrintErrors();
    return 0;
}
#endif

/*
Component Function:  void InitErrorTracer(void)
Arguments:  None
returns: None
Description:
Initialize Error tracer module.
Version : 0.1
 */
void InitErrorTracer(void)
{
    ErrorsList.clear();
}

/*
Component Function: void ErrorTracer( UINT32 ErrorCode, UINT32 LineNumber, UINT32 ColumnNumber)
Arguments:  Erro code, column and line number
returns: None
Description:
Adds Error from module with Error code to a vector of strings which contains total Errors ever captured.
Version : 0.2
0.3 Added strings to the function so that error codes can be used to generate strings in errors.
 */
void ErrorTracer( UINT32 ErrorCode, UINT32 LineNumber, UINT32 ColumnNumber)
{
    std::string Error = "Error:";
    switch (ErrorCode)
    {
        case MISSING_COMMENT:
            Error += "01 ";
            Error+="Missing Comment or incorrect number of comments";
            break;
        case INCORRECT_DATA_DECLARATION:
            Error += "02 ";
            Error+="Incorrect Data declaration";
            break;
        case INCORRECT_DATA_DEFINITION:
            Error += "03 ";
            Error+="Incorrect Data definition";
            break;
        case MISSING_FUNCTION_ARGS:
            Error += "04 ";
            Error+="Missing or incorrect function arguments";
            break;
        case INCORRECT_FUNCTION_DEFINITION:
            Error += "05 ";
            Error+="Incorrect function definition";
            break;
        case ILLEGAL_CHARACTER:
            Error += "06 ";
            Error+="Illegal character";
            break;
        case MISSING_LEFT_PARENTHESIS:
            Error += "07 ";
            Error+="Extra right parenthesis or Missing left parenthesis for right parenthesis";
            break;
        case MISSING_RIGHT_PARENTHESIS:
            Error += "08 ";
            Error="Extra left parenthesis or Missing right parenthesis for left parenthesis";
            break;
        case INVALID_IDENT_KEYWORD_USED:
            Error += "09 ";
            Error+="Cannot use keywordd as Identififer";
            break;
        case STACK_OVERFLOW:
            Error += "10 ";
            Error+="Stack overflow";
            break;
        case MEMORY_ALLOCATION_ERROR:
            Error += "11 ";
            Error+="Memory allocation Error while allocating space for operator or operand";
            break;
        case NUMBER_OUT_OF_RANGE:
            Error += "12 ";
            Error+="Number out of range";
            break;
        case UNABLE_TO_PROCESS_LINENUMBER:
            Error += "13 ";
            Error+="Too many lines to process. The program supports 100000000 lines per source file";
            break;
        case DEALLOCATING_MEMORY_FOR_DELETED_NODE:
            Error += "14 ";
            Error+="Deallocating memory for already deleted node";
            break;
        case CONSECUTIVE_MULTIPLICATION_OPERATORS:
            Error += "16 ";
            Error+="Consecutive * operators in expression ";
            break;
        case CONSECUTIVE_DIVIDE_OPERATORS:
            Error += "17 ";
            Error+="Consecutive / operators in expression ";
            break;
            // if there are operators as /* or */ or // or ** 
        case INCORRECT_ARITHMETIC:
            Error += "18 ";
            Error+="Incorrect arithmetic";
            break;
        case STACK_ACCESS_ERROR:
            Error += "19 ";
            Error+="Stack access Error-unable to store or retrieve data";
            break;	
        case MISSING_OPERAND:
		    Error += "20 ";     
		    Error+="Missing operand for OPERATOR";
		    break;
		case MISSING_OPERATOR:
		    Error += "21 ";     
		    Error+="Missing operator for operand";
		    break;
		case EMPTY_PARENTHESES:
		    Error += "22 ";
		    Error+="Empty parentheses";
		    break;
		case EMPTY_EXPRESSION:
		    Error += "23 ";
			Error+="Empty expression";
			break;
		case CONSECUTIVE_MODULUS_OPERATORS:
		   	Error += "24 ";
		   	Error+="Consecutive % operators in expression ";
		   	break;
        case MODULUS_OPERATOR_ON_FLOAT_NUMBERS:
        	Error += "25 ";
        	Error+="Modulo operator % can't be applied on floating point numbers";
        case CONSECUTIVE_EXPONENT_OPERATORS:
		   	Error += "26 ";
		   	Error+="Consecutive ^ operators in expression ";
		   	break;
		case BITWISEARITHMETIC_ON_FLOATINGPOINTNUMBERS:
			Error += "27 ";
			Error+="Bitwise arithmetic on floating point numbers";
			break;
		case MACRO_DEFINITION_DECLARATION_DONT_MATCH:
			Error += "28 ";
			Error+="Macro Definition and declaration don't match";
			break;
		case  MACRO_DEFINITION_EXPRESSION_DONT_MATCH:
			Error += "29 ";
			Error+="Macro Definition and expression don't match";
			break;	
		case MACRO_MISSING_IDENTS:
			Error += "30 ";
			Error+="Missing identifiers in Macro Definition";
			break;	
		case MACRO_MISSING_OPERANDS:
			Error += "31 ";
			Error+="Missing operands in Macro Declaration";
			break;	
		case MACRO_MISSING_DEFINITION:
			Error += "32 ";
			Error+="Missing macro definition";
			break;		
		case MACRO_MISSING_DECLARATION:
			Error += "33 ";
			Error+="Missing Macro Declaration";
			break;
		case NAME_TOOLONG:
			Error += "34 ";
			Error+="Identifer or function name too long";
			break;	
	    case IDENTIFER_IS_STATIC_ACCESSED_IN_OTHERTU:
	    	Error += "35 ";
			Error+="static symbol accesed in other TU";
			break;	
        default:
            break;
    }
    if (LineNumber!=ZERO && ColumnNumber!=ZERO)
    {
    	Error += " at line number: ";
    	Error += itostring(LineNumber);
    	Error += " column number: ";
    	Error += itostring(ColumnNumber);
	}
    ErrorsList.push_back(Error);
}

/*
Component Function: UINT32 CheckforErrors(void)
Arguments:  None
returns: None
Description:
Checks if Errors are present and returns Errors present or not present.
Version : 0.1
 */
UINT32 CheckforErrors(void)
{
    // if the vector is not empty, print all Errors return an Error code 
    UINT32 NumofErrors = ErrorsList.size();
    UINT32 ErrorType= NO_ERRORS;
    if (NumofErrors >= 1)
    {
        ErrorType= COMPILATION_ERRORS;
    }// if the vector empty then return a no Error code.
    else
    {
         ErrorType=  NO_ERRORS;
    }
    return ErrorType;
}

/*
Component Function: void PrintErrors(void)
Arguments:  None
returns: None
Description:
Displays Errors present on a console.
Version : 0.1
 */
void PrintErrors(void)
{
    UINT32 index = 0;
    // Print all Errors 
    for (index=0; index < ErrorsList.size(); index++)
    {
        std::cout << ErrorsList[index] << std::endl ;
    }

}


