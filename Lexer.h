/*
Name :  Lexer.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:
Bug and revision history;
0.1 Initial version
*/

#include "std_types.h"

/* Token types */
#define Token_identifier 1
#define Token_keyword 2
#define Token_separator 3
#define Token_operator 4
#define Token_literal 5
#define Token_comment 6   // This may be not necessary as preprocessor removes comments in all headers and source files. 

/* Token definition */
typedef struct Token_tag
{
	SCHAR* name;
	std::size_t linenumber;
	std::size_t columnnumber;
	std::size_t Type;
	Variant Data;
} Token_tag;