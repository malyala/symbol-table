# Symbol table for C compiler

This project written in C++ creates a symbol table for a C compiler. The project scans a source file and extracts symbols and inserts them into a symbol 
table to be read by the parser. Symbols are read in a source file one by one and inserted into symbol table after being categorized as identifier.  

The project has four parts:
Lexer
Symbol table 
Token processor
Parser (driver)

The parser used here will be just a stub to use functionality of symbol table. 

Copyright Amit Malyala, 2019, Andhra 