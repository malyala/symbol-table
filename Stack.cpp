
/*
Name: Stack.cpp
Copyright: Amit Malyala, 2016. All rights reserved.
Author: Amit Malyala
Date: 30-06-19
Version:
0.01 Initial version

Reference:
Foundations of computer science - Ullman and Aho

Description:
Stack implementation using a vector
To do:

Notes:

*/

#include "std_types.h"
#include <iostream>
#include "stack.h"
#include <vector>
#include "Error.h"


// create a stack of unsigned long ints for storing current scope
static  std::vector <UINT64> ScopeStack;

/* Comment to integrate with the project, UnComment to test */
#if(0)
SINT32 main(void)
{
    /* General Init functions for stacks */
    InitScopeStack();
    if (CheckforErrors())
    {
        PrintErrors();
    }
    return 0;
}
#endif

/*
Component Function: void InitScopeStack(void)
Arguments:  None
Returns: None
Description:  Initializes scope stack
Version: 0.1 Initial version
 */
void InitScopeStack(void)
{
    ClearScopeStack();
}

/*
Component Function: void ClearScopeStack(void)
Arguments:  None
Returns: None
Description:  Clears Scope Stack.
Version : 0.1
 */
void ClearScopeStack(void)
{
    ScopeStack.clear();
}


/*
Component Function: BOOL isScopeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isScopeStackEmpty(void)
{
    return ScopeStack.empty();
}

/*
Component UINT64 GetTopinScopeStack(void)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns top value of stack or returns 0 if stack empty.
Version : 0.1
 */
UINT64 GetTopinScopeStack(void)
{
    UINT64 Value=0;
    if (ScopeStack.empty())
    {
        Value=0;
    }
    else
    {
        Value = ScopeStack.back();
    }
    return Value;
}

/*
Component BOOL PopScopeStack(UINT64& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopScopeStack(UINT64& px)
{
    BOOL ReturnValue = false;
    if (ScopeStack.empty())
    {
        ReturnValue = false;
    }
    else
    {
        px = ScopeStack.back();
        ScopeStack.pop_back();
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: BOOL PushScopeStack(const UINT64& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushScopeStack(const UINT64& px)
{
    BOOL ReturnValue = false;
    if (ScopeStack.size()==9999100)
    {
        ErrorTracer(STACK_ACCESS_ERROR, 0,0);
        ReturnValue = false;
    }
    else
    {
        ScopeStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}

