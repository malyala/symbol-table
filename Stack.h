/*
Name:Stack.h
Copyright:
Author:Amit Malyala
Date: 07-01-17 19:59
Description:
*/

#ifndef STACK_H
#define STACK_H

#include "std_types.h"
#include <stack>

/*
Component Function: void InitScopeStack(void)
Arguments:  None
Returns: None
Description:  Initializes scope stack 
Version: 0.1 Initial version
 */
void InitScopeStack(void);

/*
Component Function: void ClearScopeStack(void)
Arguments:  None
Returns: None
Description:  Clears Scope Stack.
Version : 0.1
 */
void ClearScopeStack(void);

/*
Component Function: BOOL PushScopeStack(const UINT64& px);
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushScopeStack(const UINT64& px);

/*
Component BOOL PopScopeStack(UINT64& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopScopeStack(UINT64& px);


/*
Component Function: BOOL isScopeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isScopeStackEmpty(void);


/*
Component function: UINT64 GetTopinScopeStack(void)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
UINT64 GetTopinScopeStack(void);
#endif