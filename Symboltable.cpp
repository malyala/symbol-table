
/*
Coding Log:
25-10-18 Project macro processor taken as baseline. This project has to be rewritten for lexer and parser part.
         project compiles with C++11 compiler. 
29-10-18 Created symbol entry definition by merging variant with it. 
30-10-18 Created TokenProcesor function layout. Created Keywords[] array and IsKeyword() function.
31-10-18 Created TestisNum() function to detect numeric strings.char string to numeric conversion would be done by parser or Token processor. 
01-11-18  Created isalphanumeric, isalphabet() functions.
07-11-18 Modified symbol entry in symbol table. Added double linked list program for creating a list of hashtables.
11-11-18 Created double linked list for charstring and its functions InitList, insertatEndofList, getcharinCharstr() in List.cpp
13-11-18 Created linked list for static variables and functions. --Use this or add  static variables,,functions in the list of hash tables.         
23-06-19 Created a string hash function for converting a char string into a unsigned int.         
29-06-19 Created symbol entry class, symbol table class, hash table.
30-06-19 Created Scope stack functions in stack module.
02-07-19 Added details for symbol entry and symbol table classes. Created constructiors and destructors.  Edited insertSymbol() function.
05-07-19 Added set and get function for configuration and retrival of data for symbolentry and  symboltable classes.
         added a few entries in symboltable and symbol entry classes. Enabled data hiding and abstraction.
         Changed char string pointer type for name in SymbolEntry class and in Member functions.
10-07-19 Added InitSymbolTable() function for initializing linked list of symbol tables. Added error code for long ident and function names.
12-07-19 Created InsertSymbol() function. Changed error module. 
13-07-19 Created Memory allocation error detection code in InsertSymbol() function. Added pSentinelnode to make inserts of new scopes faster.
         Started Deletesymbol() function.
15-07-19 Created CreateScope() and ExitScope() functions. Added UpdateSymbol() and DeleteSymbol() functions. 
17-07-19 Edited DeleteSymbol, Insertsymbol and updatesymbol() functions.  Created isBucketElementEmpty() and SetBucketElementNotEmpty() functions 
         in SymbolEntry class and updated InsertSymbol(), LookupSymbol() functions
18-07-18 Added checking for valid symbolTable without a break for symboltable operations. Started a CreatesymbolTablebreak() function.
        Added a PrintSymbolTables() function.
        
25-07-19 Changed InsertSymbol) and DeleteSymbol() functions.        
27-07-19 Created SetSymbolData() function. Removed debugging comments.  Added dictionary module and checked hash table with 100k dictionary words.
28-07-19 Created NumberHash() function to hash a number into 0 to 4096 range.Created SymbolTableScope() function
         Added ExitScope() function for deleting scopes.Added LookupSymbol) function. 
29-07-19 Created Testcases for newer Enter and Exit scope functions.
30-07-19 Created test case for NewInsertSymbol() function.
31-07-19 Deleted previous list implementation of scopes. Changed variant type. Removed identifier in it. Created TestUpdateSymbol Test case. 
         Added ChangeSymbolData() function, PrintSybolData() functions.
01-08-19 Changed UpdateSymbol() functions and SymbolEntry::SetSymbolData() functions. 
05-08-19 Changed InsertSymbol() function . Changed insertion in single linked list. Newest node is first to lookup.
07-08-19 Changed hash function for symbol tables. Created implicit scope numbers for matching a scope with hash function for symbol tables.This 
         algorithm is to be designed.\
09-08-19 Created GetTopScopeStack() function for scope retrieval during parsing. Created BSTNode class for symbols inside a function. 
11-08-19 Created getRandomInt() and GenerateRandomCharstring() functions.These two functions create char strings from 2 to 32 chars in length. 
         Created WriteFile funtion to create a text file of random char strings using these two functions.
         Updated type casts in TestFramework.cpp 
12-08-19 Modified BSTNode class. Added new member functions.   
13-08-19 Created ListofSCopes class and added its member functions. Created Insert() and Lookup() functions for BSTNode class.
14-08-19 Modified Insert() function and BSTNode class.Created a Test case for insert function. Changed lookup() symbol to compare hash values and char strings.
15-08-19 Changed ListofScopes class. Added member functions. Corrected LookupNode() function.  Editing DeleteNode() function.
16-08-19 Changed DeleteNode() function.
17-08-19 Added new functions for lookup, insert, delete,modify using symbol char strings.  
18-08-19 Changed ModifyNode() function.
19-08-19 Created ArrayofScopes class and its member functions 
20-08-19 Changed ArrayofScopes class and member functions. Changed its types.
21-08-19 Created Scopes class member. Created InsertSymbol and LookupSymbol functions  Changed return type of LookupSymbol() function.
22-08-19 Changed return type of lookup symbol as a alternative function. Created UpdateSymbol() function. Created DeleteSymbol() function.
24-08-19 Created Scopes  and FileScope classes and their member functions. Added name attribute for major scopes in functoins or global scopes 
         Commented out BST class and member funcitons as we are not using binary search trees.
         Created InsertScope, LookupScope, DeleteScopeinFile functions.
26-08-19 Changed standalone functions into member functions in Scopes and FileScope classes. Changed member functions, deleted function parameters as 
         standalone functions are now meberfunctions. Created this pointer in member functions.
28-08-19 Renamed EnterGlobalScope() function as EnterFileScope(). The data structures for symboltables seem complete.
31-08-19 Created linked list node for connecting different file scopes. Added Member functions.
01-09-19 Created InitFileScopeList and Add node functions for FileScopenode class. Created Constructor, destructor and a function for adding nodes.
02-09-19 Created  LookupSymbolinFileScope() function.
04-09-19 Removed BST functionality. Edited Scopes::insertSymbol() function to set has static flag in symbol table of a scope and array of scopes itself.
06-09-19 Created Lexer token type and token data structure.
20-09-19 removed older symbol table functionality. Merged into mainline.


To do:
*/

#include "Symboltable.h"
#include <iostream>
/* Load dictionary list */
extern std::vector <std::string > Dictionary;
extern std::vector <std::string> Datastrings;

/* Initialize file scope list */
static FileScopeNode* pHeadNode = nullptr; 


// Comment to integrate. Uncomment to develop and test module.

SINT32 main(void)
{


	auto start = std::chrono::steady_clock::now();

	


	auto end = std::chrono::steady_clock::now();
	auto diff = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	std::cout << "Execution time : " << diff << " microseconds" << std::endl;
	return 0;
}

/**************** Hash table function code *************************/

/*
 Component Function: inline UINT32 StringHash(char const* str)
 Arguments:  String to be hashed
 Description: This function converts a char string into  a number and returns it as unsigned int.
 Returns: string hash as uint.
 Version : 0.1 Initial version with XOR
		   0.2 Added shift of mask,char and weight
		   0.3 Reduced a few arithmetic operations.
		   0.4 Removed index variable. Removed function call to strlength()
 Notes:
 */
inline UINT32 StringHash(char const* str)
{
	UINT32 mask = 255;
	UINT32 weight = 3;
	UINT32 Hashvalue = 0;
	while (*str)
	{
		/* This seems to have good distribution of hash values for different strings.*/
		Hashvalue += (((mask << 3) ^ (*str)) + ((*str) >> 1) + weight);
		str++;
	}
	return (Hashvalue % (MINIHASHTABLESIZE));
}

/*
 Component Function: inline UINT32 NumberHash(UINT64& Number)
 Arguments:  Number to be hashed
 Description: This function converts a number into another number between 0 and 4096.
 Returns: string hash as uint.
 Version : 0.1 Initial version with XOR
Notes:
This functioh seems to generate hash values with increments of 1 or 2 for linearly inreasing
seeds with a difference of 1. This can be used for scope algorithm.
 */
inline UINT32 NumberHash(UINT64& Number)
{
	return ((((HASHSYMBOLTABLESSIZE << 3) ^ Number) + ((Number) >> 2) + 3) % (HASHSYMBOLTABLESSIZE - 1));
}

/* constructor for symbol Entry */
SymbolEntry::SymbolEntry()
{
	Type = NOTADATATYPE;
	name = nullptr;
	Size = 0;
	MemoryAddress = 0;
	StorageClass = 0;
	Reserved1 = 0;
	Section = 0;
	Reserved1 = 0;
	isConst = false;
	isStatic = false;
	pNext = nullptr;
	Data.u.cString = nullptr;
	linenumber=0;
	columnnumber=0;
}

/* Destructor for symbol Entry */
SymbolEntry::~SymbolEntry()
{
	if (!isStatic)
	{
		/* Delete all memory allocated */
		if (name)
		{
			delete[](name);
			name = nullptr;
		}
		/* Edit this section */
		if (Type == STRINGTYPE)
		{
			/* Code to be added for c string*/
			if (Data.u.cString)
			{
				char *ptr = Data.u.cString;
				delete[] ptr;
				ptr = nullptr;
			}
		}
	}

}
void SymbolEntry::SetType(UINT32 SymbolType)
{
	Type = SymbolType;
}

UINT32 SymbolEntry::GetType(void) const
{
	return Type;
}


void SymbolEntry::SetConst(BOOL value)
{
	isConst = value;
}

BOOL SymbolEntry::isConstant(void) const
{
	return isConst;
}


/* Get data */
void  SymbolEntry::SetData(const Variant &SymbolData)
{
	switch (Type)
	{
	case SHORTTYPE:
		Data.u.shortValue = 0;
		break;
	case USHORTTYPE:
		Data.u.ushortValue = 0;
		break;
	case INTTYPE:
		Data.u.intValue = 0;
		break;
	case UINTTYPE:
		Data.u.uintValue = 0;
		break;
	case LONGTYPE:
		Data.u.longValue = 0;
		break;
	case ULONGTYPE:
		Data.u.ulongValue = 0;
		break;
	case CHARTYPE:
		Data.u.charValue = 0;
		break;
	case UCHARTYPE:
		Data.u.ucharValue = 0;
		break;
	case FLOATTYPE:
		Data.u.floatValue = 0;
		break;
	case DOUBLETYPE:
		Data.u.doubleValue = 0;
		break;
	case LONGDOUBLETYPE:
		Data.u.longdoubleValue = 0;
		break;
	case STRINGTYPE:
		break;
	default:
		break;
	}

	if (Type == STRINGTYPE)
	{
		/* Pointer going out of scope because memory allocated within if { }?*/
		char *ptr = Data.u.cString;
		if (ptr)
		{
			delete[] ptr;
			ptr = Data.u.cString = nullptr;
		}
		UINT32 index = 0;
		char * src = SymbolData.u.cString;
		UINT32 Length = strlength(src);
		ptr = new char[Length + 1];

		for (index = 0; index < Length; index++)
		{
			ptr[index] = src[index];
		}
		ptr[index] = '\0';


		Data.u.cString = ptr;
	}
	else
	{
		Data = SymbolData;
	}
}
/* Get data */
Variant SymbolEntry::GetData(void) const
{
	return Data;
}

/* get symbol name*/
char* SymbolEntry::GetName(void) const
{
	return name;
}

/* Set symbol name*/
/* Code edit here */
void SymbolEntry::SetName(char* str)
{
	/* Create char string using memcpy with a char* string */
	/* Check size of str in a external function */
	if (name)
	{
		delete[] name;
		name = nullptr;
	}
	UINT32 Length = strlength(str);
	UINT32 index = 0;
	name = new char[Length + 1];
	for (index = 0; index < Length; index++)
	{
		name[index] = str[index];
	}
	name[index] = '\0';
}

/* Get storage class */
UINT32 SymbolEntry::GetStorageClass(void) const
{
	return StorageClass;
}
/* Set storage class */
void SymbolEntry::SetStorageClass(UINT32 SymbolStorage)
{
	StorageClass = SymbolStorage;

}

/* Get size */
UINT32 SymbolEntry::GetSize() const
{
	return Size;
}
/* Set size */
void SymbolEntry::SetSize(UINT32 size)
{
	Size = size;
}

/* Get Memory address */
UINT32 SymbolEntry::GetMemoryAddress(void) const
{
	return MemoryAddress;
}
/* Set Memory address */
void SymbolEntry::SetMemoryAddress(UINT32 SymbolAddress)
{
	MemoryAddress = SymbolAddress;
}

/* Get Section name */
UINT32 SymbolEntry::GetSection(void) const
{
	return Section;
}
/* Set section */
void SymbolEntry::SetSection(UINT32 SymbolSection)
{
	Section = SymbolSection;
}



/* Check symbol if its static  */
BOOL SymbolEntry::isSymbolStatic(void) const
{
	return isStatic;
}
/* Set section */
void SymbolEntry::SetStatic(BOOL value)
{
	isStatic = value;
}


/* Delete data in SymbolEntry */
void SymbolEntry::DeleteData(void)
{
	if (name)
	{
		delete[] name;
		name = nullptr;
	}
	/* Edit data type */
	if (Type == STRINGTYPE)
	{
		/* Is this necessary? */
		if (Data.u.cString)
			delete (Data.u.cString);
	}
}


////////////////////////////// MemberFunctions for minihash table ////////////////////////////////////

/* Constructor for symbol Table */
SymTable::SymTable()
{
	Scope = 0;
	/* Use this to introduce breaks between symbol tables of different functions*/
	isTableFull = false;

	for (UINT32 index = 0; index < MINIHASHTABLESIZE; index++)
	{
		SymbolEntryTable[index] = nullptr;
	}


}

/* Destructor for symbol Table */
SymTable::~SymTable()
{

	/*Reorganize each bucket properly after deletion. */
}

/* Set scope in Symbol Table*/
void SymTable::SetScope(std::size_t scope)
{
	Scope = scope;
}

/* Set scope */
std::size_t SymTable::GetScope(void) const
{
	return Scope;
}

/* Check if symbol table has static member */
BOOL SymTable::ContainsStaticMember(void) const
{
	return hasStatic;
}
/* Set that table has static member */
void SymTable::SetContainsStaticMember(void)
{
	hasStatic = true;
}
/* Set hashTable is empty or not */
void SymTable::SetTableStatus(BOOL Status)
{
	isTableFull = Status;
}
/* Table status */
BOOL SymTable::getTableStatus(void) const
{
	return isTableFull;
}


/* Member functions of symbolTable class */
void SymTable::SetFirstSymbolEntry(std::size_t index, SymbolEntry *pSymbolEntry)
{
	SymbolEntryTable[index] = pSymbolEntry;
}


SymbolEntry* SymTable::GetFirstSymbolEntry(std::size_t index) const
{
	return SymbolEntryTable[index];
}

/* Constructors for scopes */
Scopes::Scopes()
{
	this->ScopeLevel = nullptr;
	this->CurrentLevel = 0;
	this->hasStaticMember = false;
	ScopeName = nullptr;
	this->Size = 0;
	this->ScopeLevel = new SymTable*[50];
	if (this->ScopeLevel)
	{
		for (std::size_t i = 0; i < 50; i++)
		{
			this->ScopeLevel[i] = nullptr;
		}
	}
	else
	{
		ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
	}
}

/* Destructors for scopes.
Being designed. */
Scopes::~Scopes()
{

}

/* Resize Scope array */

/* Change this function.*/
void Scopes::Resize(void)
{
	MoreScopes = new SymTable*[CurrentLevel + 50];
	if (MoreScopes)
	{
		for (std::size_t i = 0; i < CurrentLevel; i++)
		{
			MoreScopes[i] = ScopeLevel[i];
		}
		delete[] ScopeLevel;
		ScopeLevel = MoreScopes;
	}
	else
	{
		ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
	}
}

/* Set static member */
void Scopes::SethasStaticMember(void)
{
	hasStaticMember = true;
}

/* Get static member */
BOOL Scopes::ContainsStaticMember(void) const
{
	return hasStaticMember;
}
/* Get size */
std::size_t Scopes::GetCurrentLevel(void) const
{
	return CurrentLevel;
}

/* Get size */
void Scopes::SetCurrentLevel(std::size_t value)
{
	CurrentLevel = value;
}


/* Functions to configure SymbolTable */
void Scopes::SetSymbolTable(std::size_t index, SymTable *Table)
{
	ScopeLevel[index] = Table;
}
/* Function to get Symbol Table */
SymTable* Scopes::GetSymbolTable(std::size_t index)
{
	return ScopeLevel[index];
}


/* This gets the scope which contains global variables at array element [MINIHASHTABLESIZE]*/
ScopeNode* FileScope::GetGlobalFileScope() const
{
	return ScopeList[MINIHASHTABLESIZE];
}

/* Set major scope name*/
void Scopes::SetName(SCHAR* str)
{
	/* Create char string using memcpy with a char* string */
	/* Check size of str in a external function */
	if (ScopeName)
	{
		delete[] ScopeName;
		ScopeName = nullptr;
	}
	UINT32 Length = strlength(str);
	UINT32 index = 0;
	ScopeName = new SCHAR[Length + 1];
	for (index = 0; index < Length; index++)
	{
		ScopeName[index] = str[index];
	}
	ScopeName[index] = '\0';
}

/* Get Scope name */
char* Scopes::GetName() const
{
	return ScopeName;
}


/* Get size and set size */
void Scopes::UpdateSize(std::size_t value)
{
	Size = value;
}

std::size_t Scopes::GetScopeSize(void) const
{
	return Size;
}


/* Component Function: SymbolData Scopes::LookupSymbol(char* str)
 Arguments:  Symbol to be looked up, scope
 Returns:
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 Execution may be slow due to nested loop.
 Lookup should get recent value of that symbol.
 Return a struct with a flag and the symbol entry containing value - a string or a numeric type?
 Change the return type to pointer?
 Design - Should this function return value of a symbol or a flat that its present or not?
 Check performance with both.


 This function is looking up symbol only in current level during construction of symbol table.
 Change the function so that it looks up symbols upto Size of each Scope. Current level increases and decreases
 during construction of each scope but size will remain constant as it sthe maximum number of scopes which were
 ever constructed.
 --
 --
 --
 --
 --




 This function can be used to check lookup of definition of extern variables in other source files.
 */
SymbolData Scopes::LookupSymbol(char* str)
{
	BOOL SymbolFound = false;
	UINT32 Symbolindex = StringHash(str);
	std::size_t level = this->GetCurrentLevel();
	SymbolEntry* pCurrentSymbolEntry = nullptr;
	SymbolData SymData = { false, nullptr };
	/* Lookup symbol in scopes n to 1 */
	for (SINT32 index = level; index >= 0 && SymbolFound == false; index--)
	{
		SymTable *pCurrentTable = this->GetSymbolTable(index); /* One element lookup */
		if (pCurrentTable)
		{
			pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
			while ((pCurrentSymbolEntry) && (!SymbolFound))
			{
				if (!strcmp(str, pCurrentSymbolEntry->GetName()))
				{
					SymbolFound = true;
					SymData = { true,pCurrentSymbolEntry };

				}
				if (!SymbolFound)
				{
					pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
				}
			}
		}
	}
	return SymData;
}

/* Component Function: void UpdateSymbol(char* str, Variant& Data, UINT32& Type,std::size_t Level, UINT32& Section,UINT32& DataSize, UINT32& Address, UINT32& storage,, BOOL isSymbolStatic, BOOL isSymbolConst);
 Arguments:  Symbol to be looked up, scope    and other argument
 Returns:
 Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 Execution may be slow due to nested loop.
 Lookup should get recent value of that symbol.
  */
void Scopes::UpdateSymbol(char* str, Variant& Data, UINT32& Type, std::size_t Level, UINT32& Section, UINT32& DataSize, UINT32& Address, UINT32& storage, BOOL isSymbolStatic, BOOL isSymbolConst)
{
	BOOL SymbolUpdated = false;
	UINT32 Symbolindex = StringHash(str);
	SymbolEntry* pCurrentSymbolEntry = nullptr;
	/* Lookup symbol in scopes n to 1 */
	SymTable *pCurrentTable = this->GetSymbolTable(Level); /* One element lookup */
	if (pCurrentTable)
	{
		pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
		while ((pCurrentSymbolEntry) && (!SymbolUpdated))
		{
			if (!strcmp(str, pCurrentSymbolEntry->GetName()))
			{
				/* Set variant data here */
				SetSymbolData(pCurrentSymbolEntry, str, Data, Type, DataSize, Address, storage, Section, isSymbolStatic, isSymbolConst);
				SymbolUpdated = true;
				continue;
			}
			if (!SymbolUpdated)
			{
				pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
			}
		}
	}
}

/*
 Component Function: void Scopes::InsertSymbol(char* str, Variant &Data, UINT32 SymbolType, UINT32 SymbolSize, UINT32  SymbolAddress, UINT32 SymbolStorage, UINT32 SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  Symbol to be inserted, its data.
 Returns:
 Description: This function inserts a string into a hash table.
 Returns: None
 Version : 0.1  Initial version.
		   0.2  Changed pTemp pointer initialization with SetSymbolData() function.
		   0.3 Changed Symboltable and SCope class objects to enable hasStatic flag while inserting static symbols.
 Notes:
*/
void Scopes::InsertSymbol(char* str, Variant &Data, UINT32 SymbolType, UINT32 SymbolSize, UINT32  SymbolAddress, UINT32 SymbolStorage, UINT32 SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
{
	UINT32 SymbolEntryindex = StringHash(str);
	SymbolEntry *pCurrentSymbolEntry = nullptr;
	SymTable *pCurrentTable = this->GetSymbolTable(this->GetCurrentLevel());
	//std::cout << "string to be inserted  " << str << std::endl;
	if (!pCurrentTable)
	{
		pCurrentTable = CreateNewSymbolTable();
		this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
	}
	pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(SymbolEntryindex);
	//std::cout << "at Middle of InsertSymbol function " << std::endl;
	/* First element in bucket is empty */
	if (pCurrentSymbolEntry == nullptr)
	{
		pCurrentSymbolEntry = CreateNewSymbolEntry();
		if (pCurrentSymbolEntry)
		{
			//std::cout << "Created symbol entry node " << std::endl;
			//system("pause");
			SetSymbolData(pCurrentSymbolEntry, str, Data, SymbolType, SymbolSize, SymbolAddress, SymbolStorage, SymbolSection, isSymbolStatic, isSymbolConst);
			pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pCurrentSymbolEntry);
			if (isSymbolStatic)
			{
				if (!pCurrentTable->ContainsStaticMember())
				{
					pCurrentTable->SetContainsStaticMember();					
				}
				if (!this->ContainsStaticMember())
				{
					this->SethasStaticMember();									
				}
			}
			this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
			//std::cout << "Inserted " << pCurrentSymbolEntry->GetName() << std::endl;
			//std::cout << "Size in InsertSymbol " << pCurrentSymbolEntry->GetSize() << std::endl;
		}
		else
		{
			ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
		}
	}
	else
	{
		SymbolEntry *pTemp = CreateNewSymbolEntry();
		if (pTemp)
		{
			//std::cout << "Created symbol entry node " << std::endl;
			/* Insert a node next to first element of bucket list */
			SetSymbolData(pTemp, str, Data, SymbolType, SymbolSize, SymbolAddress, SymbolStorage, SymbolSection, isSymbolStatic, isSymbolConst);
			pTemp->pNext = pCurrentSymbolEntry;
			pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pTemp);
			if (isSymbolStatic)
			{
				if (!pCurrentTable->ContainsStaticMember())
				{
					pCurrentTable->SetContainsStaticMember();					
				}
				if (!this->ContainsStaticMember())
				{
					this->SethasStaticMember();					
				}
			}
			this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
     		//std::cout << "Inserted " << pTemp->GetName() << std::endl;
		}
		else
		{
			ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
		}
	}
}


/*
 Component Function: void DeleteSymbol(char* str)
 Arguments:  Symbol to be deleted and its scope
 Returns:
 Description: This function deletes a symbol
 Returns: None
 Version : 0.1
 Notes:
 This function should look up symbols stored in scope and delete them.
 Modify this to look up symbols from current scope to level 1 and delete them?
 Check this function for static symbol deletion.
 
*/
void Scopes::DeleteSymbol(char* str)
{
	BOOL SymbolDeleted = false;
	UINT32 SymbolEntryindex = StringHash(str);
	/* using a fuction to get pointer instead of array index */
	SymTable *pCurrentTable = this->GetSymbolTable(this->GetCurrentLevel()); /* One element lookup */
	SymbolEntry *pCurrentSymbolEntry = nullptr;
	if (pCurrentTable)
	{
		/* First element is head of bucket list */
		pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(SymbolEntryindex);
		/* pCurrentSymbolEntry is Head */
		SymbolEntry *pTemp = nullptr;
		if (pCurrentSymbolEntry)
		{
			/* First element in bucket list is the entry */
			if (!strcmp(pCurrentSymbolEntry->GetName(), str))
			{
				if (!pCurrentSymbolEntry->isSymbolStatic())
				{
					/* This list resizes itself */
				    pTemp = pCurrentSymbolEntry->pNext;
				    delete pCurrentSymbolEntry;
				    pCurrentSymbolEntry = pTemp;
				    pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pTemp);
				    SymbolDeleted = true;
				}
				
			}
			else
			{
				/* Entry is somewhere in bucket list */
				/* After deleting the entry resizes itself */
				while (pCurrentSymbolEntry->pNext && (!SymbolDeleted))
				{
					if (!pCurrentSymbolEntry->pNext->isSymbolStatic())				
					{
						if (!strcmp(str, pCurrentSymbolEntry->pNext->GetName()))
					    {
						   /* Delete symbol entry */
						   pTemp = pCurrentSymbolEntry->pNext;
						   pCurrentSymbolEntry->pNext = pCurrentSymbolEntry->pNext->pNext;
						   if (pTemp)
						    {
							    delete pTemp;
							    pTemp = nullptr;
						    }
						    SymbolDeleted = true;
						    continue;
					    }
					}
					if (!SymbolDeleted)
					{
						pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
					}
				}
			}
		}
	}
}



/*
 Component Function: void Scopes::Delete(std::size_t Level)
 Arguments:  Delete a symbol table in a level.
 Returns:
 Description: This function deletes a symbol table at a level.
 Returns: None
 Version : 0.1
 Notes:
 This is not persisitent delete function which looks up symbols stored in a scope and delete them.
 Implement this function function which deletes only symbol tables which dont have static symbols.if static symbols are present,
 don't delete that symbol table.
 Implement a function for final deletion which includes static variables when parser is done with a source file.
 */
void Scopes::Delete(std::size_t Level)
{
	SymTable *pCurrentTable = this->GetSymbolTable(Level); /* One element lookup */
	SymbolEntry *pCurrentSymbolEntry = nullptr, *pTemp = nullptr;
	if (pCurrentTable)
	{
		if (pCurrentTable->ContainsStaticMember())
		{
			for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
			{
				pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(index);
				while (pCurrentSymbolEntry)
				{
					pTemp = pCurrentSymbolEntry->pNext;
					if (!pCurrentSymbolEntry->isSymbolStatic())
					{
						delete pCurrentSymbolEntry;
						pCurrentSymbolEntry = nullptr;
					}
					pCurrentSymbolEntry = pTemp;
				}
			}
			/* Cannot delete entire symbol table as it contains a static member. */
		}
		else
		{
			for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
			{
				pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(index);
				while (pCurrentSymbolEntry)
				{
					pTemp = pCurrentSymbolEntry->pNext;
					delete pCurrentSymbolEntry;
					pCurrentSymbolEntry = nullptr;
					pCurrentSymbolEntry = pTemp;
				}
			}
			/* Delete pCurrentTable if it doesnt contain static symbol.*/
			delete pCurrentTable;
			pCurrentTable = nullptr;
			this->SetSymbolTable(Level, pCurrentTable);
		}
	}
}
/*
 Component Function: void Scopes::EnterFileScope(void)
 Arguments:  Number to be hashed
 Description: This function creates a global scope
 Returns: None
 Version : 0.1
Notes:
When entering a scope, a new symbol table is to be created for that scope.
This function is called only for scopes in functions.
Function being defined.
This function may not be necesary.
 */
void Scopes::EnterFileScope(void)
{
	this->SetCurrentLevel(0);
	SymTable *pTemp = new SymTable;
	this->SetSymbolTable(this->GetCurrentLevel(), pTemp);
}

/*
 Component Function: void Scopes::EnterLocalScope(Scopes* CurrentScope)
 Arguments:  Number to be hashed
 Description: This function creates a local scope
 Returns: None
 Version : 0.1
Notes:
When entering a scope, a new symbol table is to be created for that scope.
This function is called only for scopes in functions.
Function being defined.

Adjust size which is number of scopes here?
*/
void Scopes::EnterLocalScope(void)
{
	CurrentLevel++;
	if (CurrentLevel % 50 == 0)
	{
		this->Resize();
	}
	SymTable *pTemp = new SymTable;
	this->SetSymbolTable(this->GetCurrentLevel(), pTemp);

}

/*
 Component Function: void Scopes::ExitLocalScope(Scopes* CurrentScope)
 Arguments:  Number to be hashed
 Description: This function creates a local scope
 Returns: None
 Version : 0.1
Notes:
at scope exit,  symbol table within that scope are to be deleted except those which contain static members.
Function being defined.

Adjust size which is number of scopes here?
 */
void Scopes::ExitLocalScope(void)
{
	if (!this->ContainsStaticMember())
	{
		this->Delete(this->GetCurrentLevel());
	}
	this->CurrentLevel--;
}



/* FileScope class member functions */
/* Scope functions */
void FileScope::SetFirstScopeinBucket(ScopeNode *NewScope, std::size_t index)
{
	ScopeList[index] = NewScope;
}

/* Function to get Symbol Table */
ScopeNode* FileScope::GetFirstScopeinBucket(std::size_t index) const
{
	return ScopeList[index];
}
/* Constructor for File scope*/
FileScope::FileScope()
{
	FileName = nullptr;
	for (std::size_t i = 0; i <= MINIHASHTABLESIZE; i++)
	{
		ScopeList[i] = nullptr;
	}
	/* Change hash table to dynamic array? Do both ways and test.*/
	//pNext = nullptr;
}


FileScope::~FileScope()
{

}



/* Set major scope name*/
void FileScope::SetFileName(SCHAR* str)
{
	/* Create char string using memcpy with a char* string */
	/* Check size of str in a external function */
	if (FileName)
	{
		delete[] FileName;
		FileName = nullptr;
	}
	UINT32 Length = strlength(str);
	UINT32 index = 0;
	FileName = new SCHAR[Length + 1];
	for (index = 0; index < Length; index++)
	{
		FileName[index] = str[index];
	}
	FileName[index] = '\0';
}

/* get File name */
SCHAR* FileScope::GetFileName(void) const
{
	return FileName;
}

/////////////// End of scope functions /////////////////////////////////////

/* Function doing a single memcopy from one string to another */
void Memcopy(char* name, char* str)
{
	UINT32 Length = strlength(str);
	UINT32 index = 0;
	name = new char[Length + 1];
	for (index = 0; index < Length; index++)
	{
		name[index] = str[index];
	}
	name[index] = '\0';
}


/* Function doing a single memcopy from one string to another */
void Memcopy(char* dst, const  char* src)
{
	UINT32 Length = strlength(src);
	UINT32 index = 0;
	dst = new char[Length + 1];
	for (index = 0; index < Length; index++)
	{
		dst[index] = src[index];
	}
	dst[index] = '\0';
}

/*
 Component Function: void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section,, BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  None
 Returns:
 Description: This function prints all elements of symbol tables.
 Returns: None
 Version : 0.1
 Notes:
This function Sets symbol Data
*/
void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section, BOOL isSymbolStatic, BOOL isSymbolConst)
{
	pNode->SetData(Data);
	pNode->SetSize(Size);
	pNode->SetMemoryAddress(Address);
	pNode->SetStorageClass(storage);
	pNode->SetType(Type);
	pNode->SetName(str);
	pNode->SetSection(Section);
	pNode->SetStatic(isSymbolStatic);
	pNode->SetConst(isSymbolConst);
	pNode->pNext = nullptr;
}



/*
 Component Function: void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section)
 Arguments:  None
 Returns:
 Description: This function prints all elements of symbol tables.
 Returns: None
 Version : 0.1
 Notes:
This function Sets symbol Data
*/
void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section)
{
	pNode->SetData(Data);
	pNode->SetSize(Size);
	pNode->SetMemoryAddress(Address);
	pNode->SetStorageClass(storage);
	pNode->SetType(Type);
	pNode->SetName(str);
	pNode->SetSection(Section);
	pNode->pNext = nullptr;
}
/* Print string function */

void PrintString(SCHAR* str)
{

	std::cout << str << std::endl;
}


#if(0)


/*
Component Function: char* getcharstring(void)
Arguments:  index
Returns: char string
Description:  returns a char string.
This function would be called by getNextToken Function
This function creates a charstring and return it, if end of input stream is reached, then
nullptr is returned.
Notes:
This function would create char strings such as
+, int, name1, func, ++, ; ,
Initially ,the lexer would extract only char strings.
To do:
Create a linked list of char type to store char strings for identifers and function names and also
long strings in printf("%s", "ladkjsf;lsjf;ljf;lsfjd;lasfjld;asfj;lsfj;lsfjls fjlsdfj;lafj");

Version : 0.1
*/
char* getcharstring(unsigned long int index)
{
	char *charstring;
	static CharArray[32]; ]
	static long unsigned int size = s.size();

	while (index < size)
	{
		switch (s[index])
		{
		case SPACE:
			break;
		default:
			break;
		}

		index++;
	}
	return charstring;
}



/*
Component Function: Token getNextToken(void)
Arguments:  index
Returns: char string
Description:  returns a char string.
This function would be called by getNextToken Function
This function is
Notes:
Function being designed.

add '\0' character at the end of each char string.
This function would create char strings such as
+, int, name1, func, ++, ; ,
Initially ,the lexer would extract only char strings.
Version : 0.1
*/
Token getNextToken(void)
{
	Token CurrentToken
		Token PreviousToken;
	Token NextToken;
	static long unsigned int size index = 0;

	const char * str = getcharstring(index);

	//CurrentToken= 
	//NextToken=
	TokenProcessor(CurrentToken, nextToken, PrevToken);

	return CurrentToken;
}


/*
Component Function: void TokenProcessor(void)
Arguments:  index.
Returns: true or not
Description:  returns a char string.
This function would be called by tokenProcessor Function
This function creates a token and return it, if end of input stream is reached, then
nullptr is returned.
Notes:


The token processor would create tokens.

Each token would have the following data:

Type:
scope (Function/global/prototype)
local Scope within a function as blocks of  { }
storage class (auto or static or global)
char string containing the actual token.

The token processor function should have backtracking and look ahead features
to detect identifers and function names, symbols and operators.
The token processor also detects scope of a token within a function or in global scope.
Each of nested { } tokens would also be detected and a new scope would be assigned to them.
All identifiers detected within nested scopes would be assigned those scopes.

Function being designed.
*/
void TokenProcessor(Token &CurrToken, Token &nextToken, Token &PrevToken)
{
	unsigned int scopelevel = 0; // Assign scope to identifiers.
	static unsigned long int index = 0;
	Token = getNextToken();
}

#endif

/*
 Component Function: static std::string itostring(UINT32 a)
 Arguments:  UINT32 to be converted to std::string
 returns: None
 Description:
 Convert a decimal unsigned integer to std::string
 License: GPL V3
  Version : 0.1
To do:
itostring should have line number information.

 */
std::string itostring(const UINT32& a)
{
	std::string b;
	UINT32 NumofDigits = getNumofDecDigit(a);

	switch (NumofDigits)
	{
	case 0:
		break;

	case 1:
		b += getStrChar(a);

		break;

	case 2:
		b += getStrChar(a / 10);
		b += getStrChar(a % 10);

		break;

	case 3:
		b += getStrChar(a / 100);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);

		break;

	case 4:
		b += getStrChar(a / 1000);
		b += getStrChar((a / 100) % 10);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);

		break;

	case 5:
		b += getStrChar(a / 10000);
		b += getStrChar((a / 1000) % 10);
		b += getStrChar((a / 100) % 10);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);

		break;

	case 6:
		b += getStrChar(a / 100000);
		b += getStrChar((a / 10000) % 10);
		b += getStrChar((a / 1000) % 10);
		b += getStrChar((a / 100) % 10);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);

		break;

	case 7:
		b += getStrChar(a / 1000000);
		b += getStrChar((a / 100000) % 10);
		b += getStrChar((a / 10000) % 10);
		b += getStrChar((a / 1000) % 10);
		b += getStrChar((a / 100) % 10);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);

		break;

	case 8:
		b += getStrChar(a / 10000000);
		b += getStrChar((a / 1000000) % 10);
		b += getStrChar((a / 100000) % 10);
		b += getStrChar((a / 10000) % 10);
		b += getStrChar((a / 1000) % 10);
		b += getStrChar((a / 100) % 10);
		b += getStrChar((a / 10) % 10);
		b += getStrChar(a % 10);
		break;

	default:
		ErrorTracer(UNABLE_TO_PROCESS_LINENUMBER, 10, 1);
		break;
	}
	return b;
}


/*
 Component Function: UINT64 getNumofDecDigit(UINT64 Number)
 Arguments:  Number to detect digits
 returns: Number of digits
 Description:
 Returns number of digits in a UINT32eger
 Version : 0.1
 */

inline UINT64 getNumofDecDigit(UINT64 Number)
{
	UINT32 NumofDigits = 0;
	//Number= (Number>0)? Number: -Number; // This is always true for unsigned integers
	while (Number)
	{
		Number /= 10;
		NumofDigits++;
	}
	return NumofDigits;
}


/*
 Component Function: UCHAR getStrChar(UINT32 digit)
 Arguments:  digit as UINT32
 Returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
UCHAR getStrChar(const UINT32& digit)
{
	UCHAR StrChar = ' ';
	switch (digit)
	{
	case 0:
		StrChar = '0';
		break;
	case 1:
		StrChar = '1';
		break;
	case 2:
		StrChar = '2';
		break;
	case 3:
		StrChar = '3';
		break;
	case 4:
		StrChar = '4';
		break;
	case 5:
		StrChar = '5';
		break;
	case 6:
		StrChar = '6';
		break;
	case 7:
		StrChar = '7';
		break;
	case 8:
		StrChar = '8';
		break;
	case 9:
		StrChar = '9';
		break;
	default:
		StrChar = '0';
		break;
	}
	return StrChar;
}

/*
 Component SINT32 strlength(const SCHAR *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
SINT32 strlength(const SCHAR *str)
{
	SINT32 count = 0;
	while (*str)
	{
		count++;
		str++;
	}
	return count;
}

/*
 Component SINT32 strlength(SCHAR *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
SINT32 strlength(SCHAR *str)
{
	SINT32 count = 0;
	while (*str)
	{
		count++;
		str++;
	}
	return count;
}






/*
 Component Function: void PrintSymbolData(SymbolEntry *pSymbolEntry)
 Arguments:  Symbol entry
 Returns:
 Description: This function prints all data in a symbol including its type, memory address, section,size etc.
 Returns: None
 Version : 0.1
 Notes:
  This function is used for displaying information
*/
void PrintSymbolData(SymbolEntry *pSymbolEntry)
{
	if (pSymbolEntry->GetName())
	{
		std::cout << "Name: " << pSymbolEntry->GetName() << std::endl;
	}
	Variant Data = pSymbolEntry->GetData();

	switch (pSymbolEntry->GetType())
	{
	case SHORTTYPE:
		std::cout << "Short type: " << Data.u.shortValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case USHORTTYPE:
		std::cout << "Unsigned short type: " << Data.u.ushortValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case INTTYPE:
		std::cout << "int type: " << Data.u.intValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case UINTTYPE:
		std::cout << "unsigned int type: " << Data.u.uintValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case LONGTYPE:
		std::cout << "Long int type: " << Data.u.longValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case ULONGTYPE:
		std::cout << " unsigned Long int type: " << Data.u.ulongValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case CHARTYPE:
		std::cout << " char type: " << Data.u.charValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case UCHARTYPE:
		std::cout << "unsigned char type: " << Data.u.ucharValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case FLOATTYPE:
		std::cout << "Float type: " << Data.u.floatValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case DOUBLETYPE:
		std::cout << "Double type: " << Data.u.doubleValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case LONGDOUBLETYPE:
		std::cout << "Long Double type: " << Data.u.longdoubleValue << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	case STRINGTYPE:
		std::cout << "String type: " << Data.u.cString << std::endl;
		std::cout << "Memory address " << pSymbolEntry->GetMemoryAddress() << std::endl;
		if (pSymbolEntry->GetSection() == SECTION_DATA)
		{
			std::cout << "Section: DATA" << std::endl;
		}
		std::cout << "Size: " << pSymbolEntry->GetSize() << std::endl;
		std::cout << "Storage class: ";
		switch (pSymbolEntry->GetStorageClass())
		{
		case STORAGE_AUTO:
			std::cout << "auto" << std::endl;
			break;
		case STORAGE_STATIC:
			std::cout << "static" << std::endl;
			break;

		default:
			break;
		}
		break;
	default:
		std::cout << "Unknown type " << std::endl;
		break;

	}
}
/* Generate random integer */
std::size_t getRandomInt(void)
{
	static std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<> dis(2, 250);
	//Use dis to transform the random unsigned int generated by gen  an int in [2, 32]
	return dis(gen);
}

/* Function to generate random char string */
std::string GenerateRandomCharstring(std::size_t length)
{
	auto& characters = "0123456789"
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	std::string str;
	str.reserve(length);
	static std::mt19937 rg{ std::random_device{}() };
	std::uniform_int_distribution<std::string::size_type> pick(0, sizeof(characters) - 2);
	while (length--)
	{
		str += characters[pick(rg)];
	}
	return str;
}

///////////////////////////////// Symbol table and Scope functions //////////////////////////////////////////

/* Create new symbol table */
inline SymTable* CreateNewSymbolTable(void)
{
	//SymTable* pTemp = new SymTable;
	return new SymTable;
	//return pTemp;
}

/* Create new symbol entry */
inline SymbolEntry* CreateNewSymbolEntry(void)
{
	//SymbolEntry* pTemp = new SymbolEntry;
	return new SymbolEntry;
	//return pTemp;
}


//////////////////////////////////////////End of new Symbol table and scope functions /////////////////////////////


/* Component Function: BOOL FileScope::LookupScopeinFile(char* str)
 Arguments:  Scope to be looked up in source file.
 Returns:
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element. A scope is looked up in a hash table of scopes.
 This is for looking up function scope names
 Returns: Found or not found.
 Version : 0.1
 Notes:
*/
BOOL FileScope::LookupScopeinFile(char* str)
{
	std::size_t index = StringHash(str);
	ScopeNode *SNode = GetFirstScopeinBucket(index);
	BOOL ScopeFound = false;
	while ((SNode) && ScopeFound == false)
	{
		if (strcmp(SNode->pScope->GetName(), str) == 0)
		{
			ScopeFound = true;
		}
		SNode = SNode->pNext;
	}

	return ScopeFound;
}


/* Component Function: void FileScope::InsertScopeinFile(ScopeNode* NewScopeNode)
 Arguments:  Scope to be inserted in source file.
 Returns: None
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element. A scope is inserted in a hash table of scopes.
 Returns: none
 Version : 0.1
 Notes:
 ScopeNode is a struct which is not of type Scopes* but contains a pointer to Scopes.
*/
void FileScope::InsertScopeinFile(ScopeNode* NewScopeNode)
{
	std::size_t index = StringHash(NewScopeNode->pScope->GetName());
	ScopeNode *SNode = this->GetFirstScopeinBucket(index);
	NewScopeNode->pNext = SNode;
	this->SetFirstScopeinBucket(NewScopeNode, index);
}

/* Component Function: void FileScope::InsertGlobalScopeinFile(ScopeNode* NewScopeNode)
 Arguments:  Scope to be inserted in source file.
 Returns: None
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element. A scope is inserted in a hash table of scopes.
 Returns: none
 Version : 0.1
 Notes:
 ScopeNode is a struct which is not of type Scopes* but contains a pointer of type Scopes*.
*/
void FileScope::InsertGlobalScopeinFile(ScopeNode* NewScopeNode)
{
	this->SetFirstScopeinBucket(NewScopeNode, MINIHASHTABLESIZE);
}


/* Component Function: void FileScope::DeleteScopeinFile(char* ScopeName)
 Arguments:  Scope to be looked up and deleted
 Returns:
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element. A scope is inserted in a hash table of scopes.
 Returns: none
 Version : 0.1
 Notes:
*/
void FileScope::DeleteScopeinFile(char* ScopeName)
{
	std::size_t index = StringHash(ScopeName);
	ScopeNode *pCurrentScope = GetFirstScopeinBucket(index);
	ScopeNode *pTemp;
	BOOL ScopeDeleted = false;

	if (!strcmp(pCurrentScope->pScope->GetName(), ScopeName) == 0 && (!pCurrentScope->pScope->ContainsStaticMember()))
	{
		pTemp = pCurrentScope->pNext;
		delete pCurrentScope;
		pCurrentScope = pTemp;
		SetFirstScopeinBucket(pCurrentScope, index);
		ScopeDeleted = true;
	}
	else
	{
		while (pCurrentScope->pNext && (!ScopeDeleted))
		{
			if ((!strcmp(pCurrentScope->pNext->pScope->GetName(), ScopeName) == 0) && (!pCurrentScope->pNext->pScope->ContainsStaticMember()))
			{
				pTemp = pCurrentScope->pNext;
				pCurrentScope->pNext = pCurrentScope->pNext->pNext;
				if (pTemp)
				{
					delete pTemp;
					pTemp = nullptr;
				}
				ScopeDeleted = true;
				continue;

			}
			if (!ScopeDeleted)
			{
				pCurrentScope = pCurrentScope->pNext;
			}
		}
	}
}

/*
 Component Function: void FileScope::DeleteFileScope(void)
 Arguments:  Delete a scope
 Returns:
 Description: This function deletes a symbol table at a level.
 Returns: None
 Version : 0.1
 Notes:
 This function deletes scopes in a file if they dont contain static members.
 */
void FileScope::DeleteFileScope(void)
{
	ScopeNode* pTemp = nullptr;
	ScopeNode* pNode = nullptr;
	for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
	{
		pNode = GetFirstScopeinBucket(index);
		if (pNode)
		{
			if (pNode->pNext == nullptr)
			{
				if (!pNode->pScope->ContainsStaticMember())
				{
					delete pNode;
					pNode = nullptr;
					/* This is important to keep track of first symbol table table in a bucket*/
					SetFirstScopeinBucket(pNode, index);
				}
			}
			else
			{
				while (pNode->pNext)
				{
					if (!pNode->pNext->pScope->ContainsStaticMember())
					{
						pTemp = pNode->pNext;
						pNode->pNext = pNode->pNext->pNext;
						if (pTemp)
						{
							delete pTemp;
							pTemp = nullptr;
						}
					}
					pNode = pNode->pNext;
				}
			}
		}

	}
}


/////////////// Member functions of FileScopeNode class //////////////////////////////
/* Constructor */
FileScopeNode::FileScopeNode()
{
	pFileScope = nullptr;
	pNext = nullptr;

}
/* Destructor */
FileScopeNode::~FileScopeNode()
{

}

/* Graph/list functions connecting File scopes.
We can make a ring of nodes or just a flat tree or single linked list. */
/*
 Component Function: void FileScopeNode::SetNode(FileScope* pNode)
 Arguments:  Node to be configured
 Returns:
 Description:
 Returns: None
 Version : 0.1
 Notes:

 */
void FileScopeNode::SetNode(FileScope* pNode)
{
	pFileScope = pNode;
}

/*
 Component Function: FileScope* FileScopeNode::GetNode(void)
 Arguments: None
 Returns:
 Description:
 Returns: None
 Version : 0.1
 Notes:

 */
FileScope* FileScopeNode::GetNode(void) const
{
	return pFileScope;
}

/*
 Component Function: void AddFileScopeNode(FileScopeNode* pNode)
 Arguments:  File Scope node.
 Returns:
 Description:
 Returns: None
 Version : 0.1
 Notes:
 Addes to beginning of list after head.
 */
void AddFileScopeNode(FileScopeNode* pNode)
{
	FileScopeNode *pTemp = pHeadNode->pNext;
	pHeadNode->pNext = pNode;
	pNode->pNext = pTemp;
}

/*
Component Function: void InitFileScopeList(void);
Arguments: None
Returns:
Description:
Returns: None
Version : 0.1
Notes:
Initializes file scope list.
*/
void InitFileScopeList(void)
{
	pHeadNode = new FileScopeNode;
	pHeadNode->pNext = nullptr;
}

/*
 Component Function: const SymbolData LookupSymbolinFileScope(SymbolEntry *pSymbolEntry, SCHAR* Filename)
 Arguments: Keyword, file name
 Returns: SymbolEntry with true if found or else false and nullptr
 Description:
 Returns: None
 Version : 0.1
 Notes:
 Function being designed.
 Lookup symbol in other file scopes. This is for error detection for extern symbols.
 Current look up of symbols only searches a symbol current level which is 0 for global variables.
 
 Change the error tracer mechanism to collect all errors during execution of all functions and print them all at once. 
 Use C++ exceptions here? 
 
 The keyword string should be directly derived from SymbolEntry to detect that its a extern type.
 Then it is determined whether that ident is defined elsewhere in another TU 
 or if the variable is defined as static at which it should print a error. 
 Only when the symbol entry is extern, this look up should be performed.
 
This is done during linking? 
Try to improve this function.

Types of variables in global scope
Extern global
extern const global
global 
const global 
static (static can't be extern)

These symbols remains in scope even after detecting all symbols in a TU. The "global" tag can be removed in storage class names.
Using const for SymbolData may not optimize mov operations but it is fine for fewer number of lookups in a source file. Change to non const if needed.

*/
const SymbolData LookupSymbolinFileScope(SymbolEntry *pSymbolEntry, SCHAR* Filename)
{
	FileScopeNode* pCurrent = pHeadNode->pNext;
	SymbolData SymData = { false,nullptr };
	FileScope* pFileScope = nullptr;
	BOOL SymbolFound=false;
	while ((pCurrent) && SymbolFound==false)
	{
		pFileScope = pCurrent->GetNode();
		if (strcmp(pFileScope->GetFileName(), Filename) != 0)
		{
			ScopeNode* pCurrentScopeNode = pFileScope->GetGlobalFileScope();
			Scopes* pCurrentScope = pCurrentScopeNode->pScope;
			SymData = pCurrentScope->LookupSymbol(pSymbolEntry->GetName());
			if (SymData.isFound)
			{
			    if (pSymbolEntry->GetStorageClass() == STORAGE_EXTERN_GLOBAL)
				{
					/* Check if both symbols are defined as const or not. More of this feature to be added in code. */
					if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL)
					{
						SymbolFound=true;						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL_CONST)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_STATIC)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_EXTERN_GLOBAL_CONST)
					{
												
					}
				}
				else if (pSymbolEntry->GetStorageClass() == STORAGE_EXTERN_GLOBAL_CONST)
				{
					/* Check if both symbols are defined as const or not. More of this feature to be added in code. */
					if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL_CONST)
					{
						SymbolFound=true;						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_STATIC)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_EXTERN_GLOBAL_CONST)
					{
												
					}
					
				}
				/* This may signal a error for duplicate global symbols in file scope*/
				else if (pSymbolEntry->GetStorageClass() == STORAGE_GLOBAL)
				{
					if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL_CONST)
					{
												
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_GLOBAL)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_STATIC)
					{
						
					}
					else if (SymData.pEntry->GetStorageClass() == STORAGE_EXTERN_GLOBAL_CONST)
					{
												
					}
				}
			
			}
			
		}
		pCurrent = pCurrent->pNext;
	}
	return SymData;
}

/////////////// End of Member function.