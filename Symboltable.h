#ifndef SYMBOLTABLE
#define SYMBOLTABLE

#include "std_types.h"



#include "std_types.h"
#include "Error.h"
#include <iostream>
#include <cstdint>
#include <chrono>
#include <cstdio>
#include "io.h"
#include "Parser.h"
#include <random>
#include <cstring>


/* Storage classes of identifiers */
#define STORAGE_AUTO  1
#define STORAGE_STATIC 2
#define STORAGE_GLOBAL 4
#define STORAGE_EXTERN_GLOBAL 5
#define STORAGE_STATICGLOBAL 6
#define STORAGE_EXTERN_CONST 7
#define STORAGE_CONST 8
#define STORAGE_EXTERN_GLOBAL_CONST 9
#define STORAGE_GLOBAL_CONST 10


/* Data types */
#define NOTADATATYPE 0
#define INTTYPE 1
#define UINTTYPE 2
#define CHARTYPE 3
#define UCHARTYPE 4
#define LONGTYPE 5
#define ULONGTYPE 6
#define LONGLONGTYPE 7
#define FLOATTYPE 8
#define DOUBLETYPE 9
#define LONGDOUBLETYPE 10
#define STRINGTYPE 11
#define SHORTTYPE 12
#define USHORTTYPE 13
#define FUNCTIONTYPE 14
#define STRUCTTYPE 15
#define POINTERTYPE 16
#define USERDEFINEDTYPE 17
/* Define types for pointers */
/* Define pointer levels for all types of pointers. It is infinite or the max range of unsigned long int available */
#define POINTERLEVELS  4294967295 UL


constexpr UINT32 SECTION_DATA = 1;
constexpr UINT32 SECTION_BSS = 2;
constexpr UINT32 SECTION_CONST = 3;

/*
Component Function: char* CreateCharString(void)
Arguments:
Returns: a char string.
Description:  returns a char string.
*/
char* CreateCharString(void);

/*
 Component Function: inline UINT32 StringHash(char const* str)
 Arguments:  String to be hashed
 Returns:
 Description: This function converts a string into  a number and returns it as unsigned int.
 Returns: string hash as uint.
 Version : 0.1
 Notes:

 For now we will use this hash function.
 */
inline UINT32 StringHash(char const* str);

/* Use of #define constants is deprecated in C++14/C++17 */
/* Define size of each bucket list. */
/* This size would be used for hash table for constructing scopes with their names*/

constexpr UINT32 MINIHASHTABLESIZE = 53; /* This can be adjusted according to memory availability*/
constexpr UINT32 HASHTABLESIZE = 151; /* This can be adjusted according to memory availability*/

/* There is not much difference in exeution time between different sizes of Hash of Symboltables for about 100k symbols in total for each source file.*/
//#define HASHSYMBOLTABLESSIZE 32537
//#define HASHSYMBOLTABLESSIZE 3251
constexpr UINT32 HASHSYMBOLTABLESSIZE = 1579;

/* Symbol Entry class */
class SymbolEntry
{
private:

	UINT32 Type;
	UINT32 Size;
	UINT32 MemoryAddress;
	UINT32 StorageClass;
	UINT32 Section;
	UINT32 Reserved1;
	char* name;
	Variant Data;
	BOOL isConst;
	BOOL isStatic;
	UINT32 linenumber;
	UINT32 columnnumber;
public:
	class SymbolEntry *pNext; /* create list of symbol bucket entries */
	/* Constructor */
	SymbolEntry();
	/* Destructor */
	~SymbolEntry();
	/* Get Type */
	UINT32 GetType(void) const;
	/* Set Type */
	void SetType(UINT32 DataType);
	/* Get data */
	Variant GetData(void) const;
	/* Set data */
	void  SetData(const Variant &SymbolData);
	/* get char string symbol name*/
	char* GetName(void) const;
	/* Set symbol name*/
	void SetName(char* str);
	/* Get storage class */
	UINT32 GetStorageClass(void) const;
	/* Set storage class */
	void SetStorageClass(UINT32 SymbolStorage);
	/* Get size */
	UINT32 GetSize(void) const;
	/* Set size */
	void SetSize(UINT32 SymbolSize);
	/* Get Memory address */
	UINT32 GetMemoryAddress(void) const;
	/* Set Memory address */
	void SetMemoryAddress(UINT32 SymbolAddress);
	/* Get Section name */
	UINT32 GetSection(void) const;
	/* Set section */
	void SetSection(UINT32 SymbolSection);
	/* Delete memory allocated */
	void DeleteData(void);
	/* Is static */
	BOOL isSymbolStatic(void) const;
	/* Set a ident is static */
	void SetStatic(BOOL value);
	/* Is it a const variable */
	BOOL isConstant(void) const;
	/* Set const or not */
	void SetConst(BOOL value);

};

/*

A composite data type to get symbolvalue
*/
struct SymbolData
{
	BOOL isFound;
	SymbolEntry *pEntry;
};




/* New class definition of a symbol table for dynamic array of scopes */
class SymTable 
{
private:
	std::size_t Scope;
	/* Use this to introduce breaks between symbol tables of different scopes? */
	/* We will have this feature for now */
	BOOL isTableFull;
	/* Check if it has static members */
	BOOL hasStatic;
	/* create a hash table. */
	SymbolEntry *SymbolEntryTable[MINIHASHTABLESIZE];
public:
	/* Constructor */
	SymTable();
	/* Destructor */
	~SymTable();
	/* Set scope */
	void SetScope(std::size_t scope);
	/* Set scope */
	std::size_t GetScope(void) const;
	/* Set is empty or not */
	void SetTableStatus(bool Status);
	/* Table status */
	BOOL getTableStatus(void) const;
	/* Configure scope to have static variables */
	BOOL ContainsStaticMember(void) const;
	/* Set symbol table to have static member */
	void SetContainsStaticMember(void);
	/* Get Symbol entry */
	SymbolEntry* GetFirstSymbolEntry(std::size_t index) const;
	/* Set Symbol Entry */
	void SetFirstSymbolEntry(std::size_t index, SymbolEntry *pSymbolEntry);
};


/* Create a data structure for list of scopes with dynamic arrays.
Each scope is a hash table.
Start with a size of 50, increase size if number of symbols are more.
Measure perfomance of this DS.
Implement this with single/double linked list and dynamic array.
*/
class Scopes
{
private:
	/* This sets level of scopes */
	std::size_t Size;
	SymTable** ScopeLevel;
	SymTable** MoreScopes;
	BOOL hasStaticMember;
	/* Level of nested scopes */
	std::size_t CurrentLevel;
	/* This can be name of a function or _global*/
	char* ScopeName;
public:
	/* Get size */
	std::size_t GetCurrentLevel(void) const;
	/* Set Size */
	void SetCurrentLevel(std::size_t value);
	/* Resize scope array */
	void Resize(void);
    /* Has sttaic member or not */
	BOOL ContainsStaticMember(void) const;
	/* Set static member */
	void SethasStaticMember(void);
	/* Get Symbol Table and set at a index.*/
	SymTable* GetSymbolTable(std::size_t index);
	void SetSymbolTable(std::size_t index, SymTable *Table);
	void UpdateSize(std::size_t Value);
	std::size_t GetScopeSize(void) const;
	/* get scope name*/
	char* GetName(void) const;
	/* Set Scope name */
	void SetName(char* str);
	/* Constructor */
	Scopes();
	/* Destructor */
	~Scopes();
	/* Component Function: SymbolData LookupSymbol(char* str)
	Arguments:  Symbol to be looked up, scope
	Returns:
	Description: This function  looks up a string in a hash table of linked lists with each linked list starting
	at each position of array element.
	Returns: Found or not found.
	Version : 0.1
	Notes:
	Execution may be slow due to nested loop.
	Lookup should get recent value of that symbol.
	Return a struct with a flag and the symbol entry containing value - a string or a numeric type?
	Change the return type to pointer?
	Design - Should this function return value of a symbol or a flat that its present or not?
	Check performance with both.
	*/
	SymbolData LookupSymbol(char* str);


	/* Component Function: void UpdateSymbol(char* str, Variant& Data, UINT32& Type,std::size_t Level, UINT32& Section,UINT32& DataSize, UINT32& Address, UINT32& storage, BOOL isSymbolStatic, BOOL isSymbolConst);
	 Arguments:  Symbol to be looked up, scope    and other argument
	 Returns:
	 Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
	 at each position of array element.
	 Returns: Found or not found.
	 Version : 0.1
	 Notes:
	 Execution may be slow due to nested loop.
	 Lookup should get recent value of that symbol.
	  */
	void UpdateSymbol(char* str, Variant& Data, UINT32& Type, std::size_t Level, UINT32& Section, UINT32& DataSize, UINT32& Address, UINT32& storage, BOOL isSymbolStatic, BOOL isSymbolConst);
	/*
	 Member Function: void InsertSymbol(char* str, Variant &Data, UINT32 SymbolType, UINT32 SymbolSize, UINT32  SymbolAddress, UINT32 SymbolStorage, UINT32 SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
	 Arguments:  Symbol to be inserted, its data.
	 Returns:
	 Description: This function inserts a string into a hash table.
	 Returns: None
	 Version : 0.1  Initial version.
			   0.2  Changed pTemp pointer initialization with    () function.
	 Notes:
	*/
	void InsertSymbol(char* str, Variant &Data, UINT32 SymbolType, UINT32 SymbolSize, UINT32  SymbolAddress, UINT32 SymbolStorage, UINT32 SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst);
	/*
	 Component Function: void DeleteSymbol(char* str)
	 Arguments:  Symbol to be deleted in Current scope
	 Returns:
	 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
	 at each position of array element. The symbols start at second element of each array element.
	 Returns: None
	 Version : 0.1
	 Notes:
	 This function should look up ymbols stored in current scope.
	 Modify this to look up symbols from current scope to level 1?
	*/
	void DeleteSymbol(char* str);

	/*
	Delete an entire scope in a level.
	*/
	void Delete(std::size_t Level);

	/*
	 Component Function: void EnterLocalScope(void)
	 Arguments:  Number to be hashed
	 Description: This function creates a local scope and makes it current scope.
	 Returns: None
	 Version : 0.1
	Notes:
	 */
	void EnterLocalScope(void);

	/*
	 Component Function: void ExitLocalScope(Scopes* CurrentScope)
	 Arguments:  Number to be hashed
	 Description: This function exits currentscope
	 Returns: None
	 Version : 0.1
	Notes:
	 */
	void ExitLocalScope(void);

	/*
	 Component Function: void Scopes::EnterFileScope(void)
	 Arguments:  Number to be hashed
	 Description: This function creates File scope and block scopes in global scope which is outside of functions.
	 Returns: None
	 Version : 0.1
	 Notes:
	 When entering a scope, a new symbol table is to be created for that scope.
	 This function is called only for scopes in functions.
	 Function being defined.
	 This function may not be necesary
	*/
	void EnterFileScope(void);
};





/* For implementing a hash table with major scopes in it*/
struct ScopeNode
{
	Scopes* pScope;
	ScopeNode *pNext;
};

/* Class for list of symbol tables for each translation unit - .c and .h files */
class FileScope
{
private:
	/* one more for global variables.*/
	ScopeNode* ScopeList[MINIHASHTABLESIZE + 1];
	SCHAR* FileName;
	/* Other data to be added.*/
	UINT32 FileId;
public:
	FileScope();
	~FileScope();
	/* get scope name*/
	SCHAR* GetFileName(void) const;
	/* Set Scope name */
	void SetFileName(SCHAR* str);
	/* Scope functions */
	void SetFirstScopeinBucket(ScopeNode *NewScope, std::size_t index);
	/* Function to get Symbol Table */
	ScopeNode* GetFirstScopeinBucket(std::size_t index) const;
    /* Insert global scope */
    void InsertGlobalScopeinFile(ScopeNode* NewScopeNode);
    /* Get global scope */
	ScopeNode* GetGlobalFileScope(void) const;


	/*This feature is implemented outside of this class */
	/* This is for linking various files together. We will use a graph or trie if not this type */
	//FileScope *pNext;
	/* Member Function: BOOL LookupScopeinFile(char* str)
	Arguments:  Scope to be looked up in source file.
	Returns:
	Description: This function  looks up a string in a hash table of linked lists with each linked list starting
	at each position of array element. A scope is looked up in a hash table of scopes.
	Returns: Found or not found.
	Version : 0.1
	Notes:
	*/
	BOOL LookupScopeinFile(char* str);
	
	/* Inserts a new scope at index */
	void InsertScopeinFile(ScopeNode* NewScopeNode);
	/* Member Function: void DeleteScopeinFile(char* ScopeName)
	 Arguments:  Scope to be looked up and deleted
	 Returns:
	 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
	 at each position of array element. A scope is inserted in a hash table of scopes.
	 Returns: none
	 Version : 0.1
	 Notes:
	*/
	void DeleteScopeinFile(char* ScopeName);

	/*
	Component Function: void DeleteFileScope(void)
	Arguments:  Delete entire scope.
	Returns:
	Description: This function deletes a symbol table at a level.
	Returns: None
	Version : 0.1
	Notes:
	This function deletes all scopes in a file if they dont contain static members.
	*/
	void DeleteFileScope(void);
};


/* A data structure for linking different file scopes. We will use a graph or linked list */
class FileScopeNode
{
private:
	FileScope* pFileScope;
public:
	FileScopeNode();
	~FileScopeNode();
	/*Set node */
	void SetNode(FileScope* pNode);
	/* Get node */
	FileScope* GetNode(void) const;
	/* For a list of nodes */
	FileScopeNode *pNext;
};


/*
Component Function: void InitFileScopeList(void);
Arguments: None
Returns:
Description:
Returns: None
Version : 0.1
Notes:
Initializes file scope list
*/
void InitFileScopeList(void);

/* Add node */
/*
Component Function: void AddFileScopeNode(FileScopeNode* pNode);
Arguments:  Delete a scope
Returns:
Description:
Returns: None
Version : 0.1
Notes:
Addes to beginning of list after phead.
*/
void AddFileScopeNode(FileScopeNode* pNode);


/*
 Component Function: inline UINT32 NumberHash(UINT64& Number)
 Arguments:  Number to be hashed
 Description: This function converts a number into another number between 0 and 4096.
 Returns: string hash as uint.
 Version : 0.1 Initial version with XOR
Notes:
 */
inline UINT32 NumberHash(UINT64& Number);
/*
 Component Function: void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section,BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  None
 Returns:
 Description: This function prints all elements of symbol tables.
 Returns: None
 Version : 0.1
 Notes:
This function Sets symbol Data
*/
void SetSymbolData(SymbolEntry *pNode, char* str, Variant &Data, UINT32 Type, UINT32 Size, UINT32 Address, UINT32 storage, UINT32 Section, BOOL isSymbolStatic, BOOL isSymbolConst);




/* Create new symbol table */
inline SymTable* CreateNewSymbolTable(void);
/* Create new symbol entry */
inline SymbolEntry* CreateNewSymbolEntry(void);
/*
 Component SINT32 strlength(const SCHAR *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
SINT32 strlength(const SCHAR *str);

/*
 Component SINT32 strlength(SCHAR *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
SINT32 strlength(SCHAR *str);




/*
 Component Function: static std::string itostring(UINT32 a)
 Arguments:  UINT32 to be converted to std::string
 returns: None
 Description:
 Convert a decimal unsigned integer to std::string
 License: GPL V3
  Version : 0.1
To do:
itostring should have line number information.

 */
std::string itostring(const UINT32& a);
/*
 Component Function: UINT64 getNumofDecDigit(UINT64 Number)
 Arguments:  Number to detect digits
 returns: Number of digits
 Description:
 Returns number of digits in a UINT32eger
 Version : 0.1
 */

inline UINT64 getNumofDecDigit(UINT64 Number);

/*
 Component Function: UCHAR getStrChar(UINT32 digit)
 Arguments:  None
 returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
UCHAR getStrChar(const UINT32& digit);


/*
 Component Function: void PrintSymbolData(SymbolEntry *pSymbolEntry)
 Arguments:  Symbol entry
 Returns:
 Description: This function prints all data in a symbol including its type, memory address, section,size etc.
 Returns: None
 Version : 0.1
 Notes:
  This function is used for displaying information
*/
void PrintSymbolData(SymbolEntry *pSymbolEntry);

/*
 Component Function: void ChangeDatainSymbol(Variant &Data, UINT32& Type, UINT32& Size, UINT32& Address,UINT32& Section, UINT32& storage, UINT64& index, UINT32& Offset)
 Arguments:  Data to be changed.
 Returns:
 Description: This function Generates random data in a identifier.
 Returns: None
 Version : 0.1
 Notes:
Function being designed.
*/
void ChangeDatainSymbol(Variant &Data, UINT32& Type, UINT32& Size, UINT32& Address, UINT32& Section, UINT32& storage, UINT64& index, UINT32& Offset);



/* Function doing a single memcopy from one string to another */
void Memcopy(char* name, char* str);


/* Function doing a single memcopy from one string to another */
void Memcopy(char* dst, const char* src);


/* Generate random integer */
std::size_t getRandomInt(void);


/* Function to generate random string */
std::string GenerateRandomCharstring(std::size_t length);



/*
Component Function: const SymbolData LookupSymbolinFileScope(SymbolEntry *pSymbolEntry, SCHAR* Filename)
Arguments: Symbol Entry, file name
Returns:
Description:
Returns: None
Version : 0.1
Notes:
Lookups symbol in other symbol tables. This is for error detection.
Reuse symbol lookup function.

This looks up symbols in all other symbol tables except the current symbol table.

Change function parameters and definition.
*/
const SymbolData LookupSymbolinFileScope(SymbolEntry *pSymbolEntry, SCHAR* Filename);




#endif