/*
Name :  TestFramework.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2016-present. All rights reserved.
Description:

Notes:

Should lexer run token scan continuously to gather all tokens into a list or it should be run on the fly.

Bug and revision history;
0.1 Initial version

Notes:
Trace exeuction for creating scopes, Inserting Symbols, Deleting symbols and printing Symbol Tables.
Run hash table functions with dictionary words.

Add a feature that Symbolentries with static symbols are not deleted and their scope exists even after deleting all scopes.


Create local scope for  every { } within a function.
There are can be two scopes - Major scope at entry of function, local scope for every  {}.
Local scopes are added to each major scope. Lookups canbe faster with symbols in local scopes looked up only in preceding local scopes
or major scope or global scope.

Change SetData() funnction in symbmol entry to use pointer to Variant Data than &. Change all functions accordingly. Check functionality
with this change or revert  back to a previous release.


Compare dynamic array of scopes and linked list of scopes in a function.
Check performance for number of symbols 1000 to 5000 within scopes 1 to 50.
Check performance for number of symbols 100 in scopes 1 to 10;

Write code for detection function symbols and this should detect multiple definitions/declarations of same function name or missing
function prototypes. This is done with a grammar expression for function definitions and function prototypes.


Create File scope and block scope for global variables, function scope for functions.
Delete all memory allocated for each symbol, symbol table, Function scope, File scope.

Write functionality to look up symbol tables of each source file and match duplicate extern symbols, static variables. Similarly, static functions
should only be accessible in the source file they are defined. This should be fast within milliseconds? These lookups should get values of extern variables
declared in a source file's symbol table.


Change error tracer module to create a vector of error entries of type struct.

*/
#include "TestFramework.h"
#include <cstdlib>
#include <cstring>
#include <cstddef>
#include <iostream>
#include "symboltable.h"







/*
Component Function: void TestScopes(void)
Arguments:  None
Returns: None
Description:  None
Notes:
Write any test code here while designing/implementing functions.
Version : 0.1
*/
void TestScopes(void)
{

#if(0)
	std::cout << " Test case 1 - Create scopes and resize scopes 20 times. Check timing." << std::endl;
	Scopes* NewScopes = new Scopes;

	for (int i = 0; i < 20; i++)
	{
		NewScopes->Resize();
	}

#endif


#if(0)
	std::cout << " Test case 2 - Create scopes, increase scope upto 221100 levels with resizing scope array. Create 221100 symbol tables and read back." << std::endl;
	std::size_t CurrentLevel = 0;
	Scopes* MyScopes = new Scopes;
	Myscopes->SetName("Func");
	//std::cout << "Current level " << MyScopes->GetCurrentLevel() << std::endl;
	SymTable* pCurrentTable = nullptr;
	std::size_t ResizeCount = 0;
	for (CurrentLevel = 0; CurrentLevel < 221100; CurrentLevel++)
	{
		MyScopes->SetCurrentLevel(CurrentLevel);
		MyScopes->UpdateSize(CurrentLevel + 1);

		/* see that resizing of the array has to be done here */
		if (CurrentLevel == (ResizeCount + 1) * 50)
		{
			MyScopes->Resize();
			ResizeCount++;
		}

		if (MyScopes->GetSymbolTable(CurrentLevel) == nullptr)
		{
			//std::cout << "Scope is empty " << std::endl;
			//std::cout << "Creating new symbol table in CurrentLevel: "<< MyScopes->GetCurrentLevel() << std::endl;
			pCurrentTable = CreateNewSymbolTable();

			pCurrentTable->SetTableStatus(1);
			MyScopes->SetSymbolTable(CurrentLevel, pCurrentTable);


		}

	}

	std::cout << "Current scope Level is " << MyScopes->GetCurrentLevel() << std::endl;
	std::size_t Count = 0;
	pCurrentTable = nullptr;
	/* Read from scope list. Get all symbol tables.*/
	for (long int i = MyScopes->GetCurrentLevel(); i >= 0; i--)
	{
		pCurrentTable = MyScopes->GetSymbolTable(i);
		if (pCurrentTable != nullptr)
		{
			Count++;
		}

	}
	std::cout << "Read " << Count << " symbol tables" << std::endl;

	/* Deleting memory allocated */
	SymTable** pTemp = MyScopes->ScopeLevel;

	delete[] pTemp;
	delete MyScopes;

	/* Test is successful */
#endif



	std::cout << "Test case 3- Test scopes and symbols as in this example. Do not parse the code,just create scopes and insert symbols into symbol tables \
	as per logic of the program and look up new symbols found and print errors if not found.After this test is successful, add symbol table functions into \
	mainline.  Parsing the code will be done in another test." << std::endl;

#if (0)
	/* An example for scope algorithm is here */
#include <iostream>
/* Always look for the symbol in recent scope and get its value where it is defined/declared in a recent scope.
It recent scope doesn't have the symbol. Look for it in outer scope or until the outer most scope or level 1.*/
/* Global scope */
	int i = 5;
	/* Function declaration */
	int func(int n);

	int main(void)
	{
		/* level 1 */
		int i = 8;
		std::cout << i << std::endl;
		{
			/* level 2 */
			int i = 20;
			std::cout << i << std::endl;
			{
				/* Level 3 */
				std::cout << i << std::endl;
				{
					/* level 4 */
					std::cout << i << std::endl;
					i += 120;
					{
						/* level 5 */
						std::cout << i << std::endl;

						float i = 139;
						std::cout << i << std::endl;

						std::cout << "Before function call: " << std::endl;

						int r = func(i);
						std::cout << r << std::endl;
					}
				}
			}
		}
		return 0;
	}


	/* Function definition */
	int func(int n)
	{
		int m = n;
		if (m > 5)
		{
			return m;
		}
		else
		{
			int h = m + 4;
			if (h > 10)
			{
				return h;
			}
			int k = h + 1;
			k += 11;
			if (k > 19)
			{
				int q = k + 12;
				if (q > 33)
				{
					return q;
				}
				else
				{
					return k;
				}
			}
			else
			{
				int l = k;
				l += 3;
				if (l > 21)
				{
					return l;
				}
			}
		}
		return m;
	}

#endif 
	/* ALgorithm for scopes begins */
	std::cout << "Test case begin" << std::endl;
	FileScope *SourceFile = new FileScope;
	SourceFile->SetFileName((char*)"main.c");

	std::cout << "File name " << SourceFile->GetFileName() << std::endl;

	ScopeNode* pScopeNode = new ScopeNode;
	pScopeNode->pScope = new Scopes;
	pScopeNode->pNext = nullptr;
	pScopeNode->pScope->SetName((char*) "Global");
	std::cout << "Current level in global scope " << pScopeNode->pScope->GetCurrentLevel() << std::endl;
	SourceFile->DeleteFileScope();
}