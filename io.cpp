/* 
Name: io.cpp
Author: Amit Malyala, Copyright AM, 2019 all rights reserved.
Version: 0.1
Description: 
These functions provide functionality of a flat Tree which is similar to a double linked list.

Notes:
Version and change history:
0.1 Initial version
0.2 Modified the file to read strings from a file.
***********************************************************************/
#include <fstream>

#include <iostream>
#include "std_types.h"
#include "io.h"

std::vector <std::string> Dictionary;

std::vector <std::string> Datastrings;


/* Comment to integrate with the project, UnComment to test */
#if(0)
SINT32 main(void)
{
	InitDictionary();
    return 0;
}
#endif

/*
Function Name : void InitDictionary(void)
Description:
Init dictionary
Arguments: None
*/
void InitDictionary(void)
{
	ReadFileintoVector("WordsEn.txt",Dictionary);
	ReadFileintoVector("Datastrings.txt",Datastrings);
	
}

/*
Function Name : void ReadFileintoVector(const std::string& Filename, std::vector<std::string>& Dict)
Description:
Read and display contents of a filename
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void ReadFileintoVector(const std::string& Filename, std::vector<std::string>& Dict)
{
  std::string line;
  
  std::ifstream myfile (Filename);
  //SCHAR c;
  if (myfile.is_open())
  {
    //while (myfile.get(c ))  // For reading bytes
    while (getline(myfile,line ))
    {
       //std::cout << c ; // Printing bytes
      //std::cout << line << std::endl ; // Print a line
      //OriginalFile +=c;
      //OriginalFile +=line;
      if (line!="")
      {
      	//std::cout << "Inserting " << line << std::endl;
      	Dict.push_back(line);
	  }
    }
    myfile.close();
  }
  else
  {
      std::cout << std::endl << "File not found";
  }
}
/*
 Component Function: void GenerateCharstringsinFile(const std::string & Filename)
 Arguments:  output file name.
 returns: None
 Description:
 writes a ascii or UTF8 char into a file
 Version : 0.1
 Notes:
 */
void GenerateCharstringsinFile(const std::string & Filename)
{
  int index=0;
  UINT64 length=0;
  std::string Datastring;
  //std::ofstream myfile(Filename,std::ios::out | std::ios::app);	
  std::ofstream myfile(Filename);	
  if (myfile.is_open())
  {
  	for (index=0;index<106000;index++)
  	{
  		
  		length = getRandomInt();
	    Datastring=GenerateRandomCharstring(length);
	    myfile << Datastring<< std::endl;
	    
	    
	}
    myfile.close();
  }
  else 
  {
  	std::cout << "Unable to open file";
  }
}