/* 
Name: io.cpp
Author: Amit Malyala
Version: 0.1
Copyright(C) Amit Malyala, 2015-Current. All rights reserved.

Description: .
These functions provide functionality of a double linked list.

Notes:
Measure performance of the program for insertion at end of list on a dictionary file of about 100000 dictionary words.
Measure performance of the program on randomized insertion of unsorted 100000 dictionary into a sorted list.

Version and change history:
0.1 Initial version
0.2 Modified the file to read a list of 100000 dictionary words from a file.
*/
#include <string>
#include <vector>
#include "symboltable.h"

/*
Function Name : void ReadFileintoVector(const std::string& Filename, std::vector<std::string>& Dict)
Description:
Read and display contents of a filename
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void ReadFileintoVector(const std::string& Filename, std::vector<std::string>& Dict);
/*
Function Name : void InitDictionary(void)
Description:
Init dictionary
Arguments: None
*/
void InitDictionary(void);
/*
 Component Function: void GenerateCharstringsinFile(const std::string & Filename)
 Arguments:  output file name.
 returns: None
 Description:
 writes a ascii or UTF8 char into a file
 Version : 0.1
 Notes:
 */
void GenerateCharstringsinFile(const std::string & Filename);